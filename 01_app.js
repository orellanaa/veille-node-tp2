/* PROJET PAR AARON ORELLANA - Travail Pratique 2 - Node Veille */

// Variables Constantes
const express = require('express');
const fs = require('fs');
const util = require("util");
const app = express();

// ---------------------- AJOUT DE LA LIBRAIRIE - SOCKET.IO ------------------- //
const server = require('http').createServer(app);
const io = require('./mes_modules/chat_socket').listen(server);


// ---------------------- AJOUT DU MODULE - MONGODB ------------------------- //
const bodyParser= require('body-parser');
const MongoClient = require('mongodb').MongoClient; // le pilote MongoDB
const ObjectID = require('mongodb').ObjectID;
app.use(bodyParser.urlencoded({extended: true}));

// ---------------------- AJOUT DU MODULE - EJS ------------------------- //
// On associe le moteur de vue au module «ejs».
app.use(express.static('public'));

// ---------------------- AJOUT DU MODULE - COOKIE-PARSER --------------- //
// Cookie Parser DOIT être EN PREMIER ! POUR QUE LES COOKIES FONCTIONNENT.
const cookieParser = require('cookie-parser');
app.use(cookieParser());


// ---------------------- AJOUT DU MODULE - i18n --------------- //
const i18n = require('i18n');

i18n.configure({ 
	locales : ['fr', 'en'],
	cookie : 'langueChoisie', 
    directory : __dirname + '/locales' 
})

app.use(i18n.init);


// ---------------------- MongoDB ----------------------------- //

let db // Variable qui contiendra le lien sur la BD.

MongoClient.connect('mongodb://127.0.0.1:27017', (err, database) => {

    if (err) return console.log(err)
    db = database.db('carnet_adresse') // Connexion a la basse de données
    console.log('connexion à la BD')


    // Lancement du serveur Express sur le port 8081.
    server.listen(8081, (err) => {
        if (err) console.log(err)
        console.log('connexion à la BD et on écoute sur le port 8081')
    })
})


// ---------------------- LES ROUTES ----------------------------- //

app.set('view engine', 'ejs'); // Générateur de template EJS


// -------------- ROUTE ANGLAIS / FRANCAIS - i18 ----------------- //


// ROUTE GLOBALE
app.get('/:locale(en|fr)',  (req, res) => {

	// On dit au navigateur de se souvenir grace aux cookies que la langue est celle du req.params.locale dans url.
	res.cookie('langueChoisie' , req.params.locale)

	// VERIFIE LE COOKIE DE LA LANGUE CHOISE
	console.log('Cookies: la langue est', req.cookies.langueChoisie)

	// on récupère le paramètre de l'url pour enregistrer la langue
	res.setLocale(req.params.locale)

	// Envoi a l'acueil.ejs
	//res.render('accueil.ejs')

	// Permet de rester sur la même page 
    res.redirect(req.get('referer'));
    
  })

// ---------------------- LA ROUTE - /ACCUEIL ---------------------- //

app.get('/', function (req, res) {

 res.render('accueil.ejs')  
 
});

// ---------------------- LA ROUTE - /ADRESSE ----------------------- //

app.get('/adresse', function (req, res) {

   var cursor = db.collection('adresse')
                .find().toArray(function(err, resultat){
                    if (err) return console.log(err)        
                    res.render('adresse.ejs', {adresses: resultat})   
            });
})

// -------------- LA ROUTE - /RECHERCHER ----------------------------- //

app.post('/rechercher',  (req, res) => {

})

// ---------------------- LA ROUTE - /AJOUTER ------------------------ //

app.post('/ajouter', (req, res) => {

    console.log('route /ajouter')	

    db.collection('adresse').save(req.body, (err, result) => {
        if (err) return console.log(err)	
        console.log('sauvegarder dans la BD')
        res.redirect('/adresse')
    })
})

// ---------------------- LA ROUTE - /MODIFIER ------------------------ //

app.post('/modifier', (req, res) => {

    console.log('route /modifier')

    // console.log('util = ' + util.inspect(req.body));
    req.body._id = 	ObjectID(req.body._id)
    db.collection('adresse').save(req.body, (err, result) => {
            if (err) return console.log(err)
            console.log('sauvegarder dans la BD')
            res.redirect('/adresse')
        })
})

// ---------------------- LA ROUTE - /DETRUIRE ------------------------ //

app.get('/detruire/:id', (req, res) => {

    console.log('route /detruire')
    // console.log('util = ' + util.inspect(req.params));	

    var id = req.params.id

    console.log(id)

    db.collection('adresse')
    .findOneAndDelete({"_id": ObjectID(req.params.id)}, (err, resultat) => {
        if (err) return console.log(err)
        res.redirect('/adresse')  // redirige vers la route qui affiche la collection
    })
})


// ---------------------- LA ROUTE - /TRIER ------------------------ //
app.get('/trier/:cle/:ordre', (req, res) => {

    let cle = req.params.cle
    let ordre = (req.params.ordre == 'asc' ? 1 : -1)

    let cursor = db.collection('adresse').find().sort(cle,ordre).toArray(function(err, resultat)
    {
        ordre = (req.params.ordre == 'asc' ? 'desc' : 'asc')  
        res.render('adresse.ejs', {adresses: resultat, cle, ordre })	
    })
}) 


// ---------------------- LA ROUTE - /VIDER------------------------ //
app.get('/vider', (req, res) => {

	let cursor = db.collection('adresse').drop((err, res)=>{
		if(err) console.error(err)
			console.log('ok')	
		})
	res.redirect('/adresse')
})

// ---------------------- LA ROUTE - /CHAT ------------------------ //
app.get('/chat', (req, res) => {
	res.render('socket_vue.ejs')
})

// ---------------------- LES ROUTES - AJAX ------------------------ //

// Dans notre application serveur
// Une nouvelle route pour traiter la requête AJAX

app.post('/ajax_modifier', (req,res) => {
    req.body._id = ObjectID(req.body._id)
    console.log("req.body._id = " + req.body._id)
  
    db.collection('adresse').save(req.body, (err, result) => {
      if (err) return console.log(err)
      console.log('sauvegarder dans la BD')
      res.send(JSON.stringify(req.body));
      // res.status(204)
    })
  })
  
  
  app.post('/ajax_detruire', (req,res) => {
    db.collection('adresse').findOneAndDelete({"_id": ObjectID(req.body._id)}, (err, resultat) => {
        console.log(util.inspect(req.body))
    if (err) return console.log(err)
      res.send(JSON.stringify(req.body))  // redirige vers la route qui affiche la collection
    })
  })
  
  app.post('/ajax_ajouter', (req, res) => {
    console.log('route /ajax_ajouter') 
    db.collection('adresse').save(req.body, (err, result) => {
      if (err) return console.log(err)
      // console.log(req.body) 
      console.log('sauvegarder dans la BD')
      res.send(JSON.stringify(req.body))
    })
  })